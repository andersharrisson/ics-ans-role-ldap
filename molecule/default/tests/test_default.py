import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_slapd(host):
    service = host.service("slapd")
    assert service.is_running
    assert service.is_enabled


def test_lsc(host):
    service = host.service("lsc-unit")
    assert not service.is_running
    assert not service.is_enabled
