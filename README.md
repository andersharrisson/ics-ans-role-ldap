ics-ans-ldap
===================

Ansible playbook to install ldap.

To create a testing self-signed certificate, run:
openssl genrsa -out MyRootCA.key 2048
openssl req -x509 -new -nodes -key MyRootCA.key -sha256 -days 1024 -out MyRootCA.pem

openssl req -config openssl.conf -new -key ldap.key -out ldap.csr
openssl x509 -req -in ldap.csr -CA MyRootCA.pem -CAkey MyRootCA.key -CAcreateserial -out ldap2.crt -days 3650 -sha256



Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause

